package com.ruoyi.core.config.beetlsql;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.beetl.core.Function;
import org.beetl.sql.core.ClasspathLoader;
import org.beetl.sql.core.IDAutoGen;
import org.beetl.sql.core.Interceptor;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.core.db.OracleStyle;
import org.beetl.sql.core.engine.IsBlank;
import org.beetl.sql.ext.DebugInterceptor;
import org.beetl.sql.ext.spring4.BeetlSqlDataSource;
import org.beetl.sql.ext.spring4.BeetlSqlScannerConfigurer;
import org.beetl.sql.ext.spring4.SqlManagerFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Profiles;

import com.ruoyi.core.database.UuidAutoGen;


@Configuration
public class BeetlSqlDataSourceConfig {
	
	@Bean(name = "beetlSqlScannerConfigurer")
    public BeetlSqlScannerConfigurer getBeetlSqlScannerConfigurer() {
    	BeetlSqlScannerConfigurer conf = new BeetlSqlScannerConfigurer();
    	conf.setBasePackage("com.ruoyi");
    	conf.setDaoSuffix("Mapper");
    	conf.setSqlManagerFactoryBeanName("sqlManagerFactoryBean");
    	return conf;
    }
    
    @Bean(name = "sqlManagerFactoryBean")
    @Primary
    public SqlManagerFactoryBean getSqlManagerFactoryBean(ConfigurableApplicationContext  context ,@Autowired  DataSource datasource) {
    	SqlManagerFactoryBean factory = new SqlManagerFactoryBean();
    	
    	BeetlSqlDataSource  source = new BeetlSqlDataSource();
    	source.setMasterSource(datasource);
    	boolean debug = context.getEnvironment().acceptsProfiles(Profiles.of("debug"));
    	Properties prop = new Properties();
    	prop.setProperty("PRODUCT_MODE", ""+(!debug));
    	factory.setExtProperties(prop);
    	factory.setCs(source);
    	
    	
    	Map<String, Function> functions  = new HashMap<>();
    	functions.put("isBlank", new IsBlank());
    	factory.setFunctions(functions);
    	
    	if(context.getEnvironment().acceptsProfiles(Profiles.of("oracle"))) {
    		factory.setDbStyle(new OracleStyle());
    	}else if(context.getEnvironment().acceptsProfiles(Profiles.of("sqlserver"))) {
    		factory.setDbStyle(new BeetlSqlServer2012Style());
    	}else {
    		factory.setDbStyle(new MySqlStyle());
    	}
    	
    	factory.setNc(new RuoyiNameConversion());;
    	
    	if(debug) {
    		Interceptor interceptor = new DebugInterceptor();
        	factory.setInterceptors(new Interceptor[]{interceptor});
    	}
    	
    	Map<String, IDAutoGen> idAutoGens = new HashMap<String, IDAutoGen>();
    	idAutoGens.put("uuid", new UuidAutoGen());
    	factory.setIdAutoGens(idAutoGens);
    	factory.setSqlLoader(new ClasspathLoader("/sql"));
    	return factory;
    }

    
}

