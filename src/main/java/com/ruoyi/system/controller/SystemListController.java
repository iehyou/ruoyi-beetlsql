package com.ruoyi.system.controller;

import java.util.Date;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.core.web.PrimitiveController;
import com.ruoyi.system.domain.SysConfig;
import com.ruoyi.system.domain.SysDictData;
import com.ruoyi.system.domain.SysDictType;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.domain.SysPost;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.SysConfigMapper;
import com.ruoyi.system.mapper.SysDictDataMapper;
import com.ruoyi.system.mapper.SysDictTypeMapper;
import com.ruoyi.system.mapper.SysNoticeMapper;
import com.ruoyi.system.mapper.SysPostMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.SystemListService;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

@Controller
@RequestMapping("/system/list")
public class SystemListController extends PrimitiveController{
	
	@Autowired
	SysUserMapper sysUserMapper;

	@Autowired
	SysDictDataMapper sysDictDataMapper;
	
	@Autowired
	SysDictTypeMapper sysDictTypeMapper;
	
	@Autowired
	SysPostMapper sysPostMapper;
	
	@Autowired
	SysConfigMapper sysConfigMapper;

	@Autowired
	SysNoticeMapper sysNoticeMapper;
	@Autowired
	SystemListService systemListService;
	
	@RequiresPermissions("system:user:list")
    @PostMapping("/user")
    @ResponseBody
    public TableDataInfo user(@RequestParam Map<String,Object> params){
		BaseEntity baseEntity = new BaseEntity();
        return getDataTable(systemListService.selectUserList(baseEntity , params));
        
    }
	
	
	@RequiresPermissions("system:dict:list")
    @PostMapping("/dict")
    @ResponseBody
    public TableDataInfo dict(@RequestParam Map<String,Object> params){
		PageQuery<SysDictType> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		String end = (String) params.remove("params[endTime]");
		params.put("endTime", StrUtil.isBlank(end)? null:new Date(DateUtil.parseDate(end).getTime()));
		
		String begin = (String) params.remove("params[beginTime]");
		params.put("beginTime", StrUtil.isBlank(begin)? null:new Date(DateUtil.parseDate(begin).getTime()));
		pageQuery.setParas(params);
        return getDataTable(sysDictTypeMapper.queryByCondition(pageQuery));
    }
	
	
	@RequiresPermissions("system:dict:list")
    @PostMapping("/dict/data")
    @ResponseBody
    public TableDataInfo dictdata(@RequestParam Map<String,Object> params){
		PageQuery<SysDictData> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		String end = (String) params.remove("params[endTime]");
		params.put("endTime", StrUtil.isBlank(end)? null:new Date(DateUtil.parseDate(end).getTime()));
		
		String begin = (String) params.remove("params[beginTime]");
		params.put("beginTime", StrUtil.isBlank(begin)? null:new Date(DateUtil.parseDate(begin).getTime()));
		pageQuery.setParas(params);
        return getDataTable(sysDictDataMapper.queryByCondition(pageQuery));
    }
	
	
	@RequiresPermissions("system:post:list")
    @PostMapping("/post")
    @ResponseBody
    public TableDataInfo post(@RequestParam Map<String,Object> params){
		PageQuery<SysPost> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		pageQuery.setParas(params);
        return getDataTable(sysPostMapper.queryByCondition(pageQuery));
    }

	@RequiresPermissions("system:config:list")
    @PostMapping("/config")
    @ResponseBody
    public TableDataInfo config(@RequestParam Map<String,Object> params){
		PageQuery<SysConfig> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		String end = (String) params.remove("params[endTime]");
		params.put("endTime", StrUtil.isBlank(end)? null:new Date(DateUtil.parseDate(end).getTime()));
		
		String begin = (String) params.remove("params[beginTime]");
		params.put("beginTime", StrUtil.isBlank(begin)? null:new Date(DateUtil.parseDate(begin).getTime()));
		pageQuery.setParas(params);
        return getDataTable(sysConfigMapper.queryByCondition(pageQuery));
    }
	
	
	@RequiresPermissions("system:notice:list")
    @PostMapping("/notice")
    @ResponseBody
    public TableDataInfo notice(@RequestParam Map<String,Object> params){
		PageQuery<SysNotice> pageQuery = new PageQuery<>();
		pageQuery.setPageNumber(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
		pageQuery.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
		String end = (String) params.remove("params[endTime]");
		params.put("endTime", StrUtil.isBlank(end)? null:new Date(DateUtil.parseDate(end).getTime()));
		
		String begin = (String) params.remove("params[beginTime]");
		params.put("beginTime", StrUtil.isBlank(begin)? null:new Date(DateUtil.parseDate(begin).getTime()));
		pageQuery.setParas(params);
        return getDataTable(sysNoticeMapper.queryByCondition(pageQuery));
    }

	
	@RequiresPermissions("system:role:list")
    @PostMapping("/role")
    @ResponseBody
    public TableDataInfo role(@RequestParam Map<String,Object> params){
		BaseEntity baseEntity = new BaseEntity();
        return getDataTable(systemListService.selectRoleList(baseEntity , params));
    }

	@RequiresPermissions("system:role:list")
    @PostMapping("/role/authuser")
    @ResponseBody
    public TableDataInfo roleauthuser(@RequestParam Map<String,Object> params){
		BaseEntity baseEntity = new BaseEntity();
        return getDataTable(systemListService.selectAuthUserList(baseEntity , params));
    }
	

	@RequiresPermissions("system:role:list")
    @PostMapping("/role/unallocated")
    @ResponseBody
    public TableDataInfo roleunallocated(@RequestParam Map<String,Object> params){
		
		BaseEntity baseEntity = new BaseEntity();
        return getDataTable(systemListService.selectUnauthUserList(baseEntity , params));
		
    }
	
	
	
}
