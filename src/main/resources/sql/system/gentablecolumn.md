queryByCondition
===
	select 
	@pageTag(){
	*
	@}
	from gen_table where 1=1
	@if(!isBlank(tableName)){
	    and  table_name like #'%'+tableName+'%'#
	@}
	@if(!isBlank(tableComment)){
	    and  table_comment like #'%'+tableComment+'%'#
	@}
	
	@pageIgnoreTag(){
	order by table_id desc
	@}

	
selectDbTableList_oracle
===
	select lower(dt.table_name) as table_name, dtc.comments as table_comment, uo.created as create_time, uo.last_ddl_time as update_time
	from user_tables dt, user_tab_comments dtc, user_objects uo
	where dt.table_name = dtc.table_name
	and dt.table_name = uo.object_name
	and uo.object_type = 'TABLE'
	AND dt.table_name NOT LIKE 'QRTZ_%' AND dt.table_name NOT LIKE 'GEN_%'
	AND lower(dt.table_name) NOT IN (select table_name from gen_table)
	
	@if(!isBlank(tableName)){
	    AND lower(dt.table_name)  like lower(#'%'+tableName+'%'#)
	@}
	@if(!isBlank(tableComment)){
	    AND lower(dt.comments)  like lower(#'%'+tableComment+'%'#)
	@}
		
	
selectDbTableList_sqlserver
===
	SELECT cast(D.NAME as nvarchar) as table_name,cast(F.VALUE as nvarchar) as table_comment,
	    crdate as create_time,refdate as update_time FROM    SYSOBJECTS   D
	inner   JOIN   SYS.EXTENDED_PROPERTIES F   ON   D.ID=F.MAJOR_ID
	AND   F.MINOR_ID=0 AND   D.XTYPE='U' AND  D.NAME!='DTPROPERTIES'
	AND D.NAME NOT IN (select table_name from gen_table)
	@if(!isBlank(tableName)){
	    AND lower(D.NAME) like lower(#'%'+tableName+'%'#)
	@}
	@if(!isBlank(tableComment)){
	   AND lower(cast(F.VALUE as nvarchar)) like lower(#'%'+tableComment+'%'#)
	@}
		

	
selectDbTableList_mysql
===
	select table_name, table_comment, create_time, update_time from information_schema.tables
	where table_schema = (select database())
	AND table_name NOT LIKE 'qrtz_%' AND table_name NOT LIKE 'gen_%'
	AND table_name NOT IN (select table_name from gen_table)
	@if(!isBlank(tableName)){
	    AND lower(table_name) like lower(concat('%', #{tableName}, '%'))
	@}
	@if(!isBlank(tableComment)){
	   AND lower(table_comment) like lower(concat('%', #{tableComment}, '%'))
	@}
	
selectDbTableListByNames_sqlserver
===
	select lower(dt.table_name) as table_name, dtc.comments as table_comment, uo.created as create_time, uo.last_ddl_time as update_time
		from user_tables dt, user_tab_comments dtc, user_objects uo
		where dt.table_name = dtc.table_name
		and dt.table_name = uo.object_name
		and uo.object_type = 'TABLE'
		AND dt.table_name NOT LIKE 'QRTZ_%' AND dt.table_name NOT LIKE 'GEN_%'
		AND dt.table_name NOT IN (select table_name from gen_table)
		and lower(dt.table_name) in (#join(tableNames)#)
		
selectDbTableListByNames_sqlserver
===
	SELECT cast(D.NAME as nvarchar) as table_name,cast(F.VALUE as nvarchar) as table_comment,
	    crdate as create_time,refdate as update_time FROM    SYSOBJECTS   D
		inner   JOIN   SYS.EXTENDED_PROPERTIES F   ON   D.ID=F.MAJOR_ID
		AND   F.MINOR_ID=0 AND   D.XTYPE='U' AND  D.NAME!='DTPROPERTIES'
		AND D.NAME in (#join(tableNames)#)
			
selectDbTableListByNames_mysql
===
	select table_name, table_comment, create_time, update_time from information_schema.tables
		where table_name NOT LIKE 'qrtz_%' and table_name NOT LIKE 'gen_%' and table_schema = (select database())
	and table_name in (#join(tableNames)#)