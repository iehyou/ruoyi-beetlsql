# ruoyi-beetlsql

#### 介绍
在不修改ruoyi任何代码的基础上，将mybatis全部改成beetlsql，并兼容sqlsever,oracle,mysql。做这个事情主要有2方便的原因
1. 熟悉springboot的开发模式，之前都是J2EE开发模式
1. 不喜欢写mybatis的XML的风格，而且支持多数据库比较烂，喜欢beetlsql风格，自由度高，少写代码

#### 目前进度：
* 2020-01-31 完成sqlserver 2012 数据库的适配，未完全测试
* 2020-01-29 完成数据隔离的适配
* 2020-01-28 已完成oracle适配，未完全测试
* 2019-11-21 已完成mysql适配，未完全测试
如有bug，请提交Issues

#### 部署步骤
1. 先按照[ruoyi](https://gitee.com/y_project/RuoYi)部署说明完成部署
1. 下载本项目，在doc下面找对应数据的ruoyi_数据库类型_modify.sql，按照说明修改
1. 下载本项目，修改application-mysql.yml的数据连接配置，其他数据库，需要修改application.yml的spring.profiles.active配置成对应数据库类型
1. 找到WebApplication.java启动完成。
